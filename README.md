## R/Pharma 2018

This repository contains slides for my presentation entitled __Developing Powerful Shiny Applications in an Enterprise Environment: Best Practices and Lessons Learned__ at the R/Pharma Conference held in Cambridge, MA on August 15-16, 2018.
